/*
EXCISE 1: TÍNH TIỀN LƯƠNG NHÂN VIÊN
    -Input: Số ngày công của nhân viên.
    -Process: 
        +Tạo biến số ngày công của nhân viên và tiền lương của nhân viên là workDay, total.
        +Tính lương theo công thức: total = workDay*100.000
    -Output: Tiền lương của nhân viên
*/

var workDay = 26;
var total;
total = workDay * 100000;
console.log(total);

/*
EXCISE 2: TÍNH GIÁ TRỊ TRUNG BÌNH
    -Input: Giá trị của 5 số thực.
    -Process: 
        +Tạo biến và gán giá trị cho 5 số thực là a,b,c,d,e.
        +Tạo biến average là giá trị trung bình.
        +Tính trung bình theo công thức: average = (a+b+c+d+e)/5.
    -Output: Giá trị trung bình của 5 số a,b,c,d,e.
*/

var a = 2,
  b = 3,
  c = 4,
  d = 5,
  e = 7;
var average;
average = (a + b + c + d + e) / 5;
console.log(average);

/*
EXCISE 3: QUY ĐỔI TIỀN
    -Input: Số tiền USD
    -Process: 
        +Tạo biến dollar và VND.
        +Tính số tiền cần quy đổi theo công thưc: VND = dollar*23.500
    -Output: Số tiền quy đổi
*/

var dollar = 10;
var VND;
VND = dollar * 23500;
console.log(VND);

/*
EXCISE 4: TÍNH DIỆN TÍCH, CHU VI HÌNH CHỮ NHẬT
    -Input: chiều dài, chiều rộng hình chữ nhật.
    -Process: 
        +Tạo biến chieuDai, chieuRong, chuVi, dienTich.
        +Tính theo công thưc: dienTich = chieuDai*chieuRong
                              chuVi = (chieuDai + chieuRong)*2
    -Output: Chu vi, diện tích hình chữ nhật.
*/

var chieuDai = 3,
  chieuRong = 4;
var chuVi, dienTich;
dienTich = chieuDai * chieuRong;
chuVi = (chieuDai + chieuRong) * 2;
console.log(dienTich);
console.log(chuVi);

/*
EXCISE 5: TÍNH TỔNG 2 KÝ SỐ
    -Input: Số tự nhiên gồm 2 chữ số
    -Process: Tính theo công thức
        +Tạo biến donVi, hangChuc, total, number.
        +Tách hàng chục: hangChuc =  Math.floor(number/10).
        +Tách hàng đơn vị: donVi = number%10.
        +Tổng: total = hangChuc + donVi
    -Output: Tổng 2 ký số
*/

var number = 63;
var hangChuc, donVi, total;

hangChuc = Math.floor(number / 10);
donVi = number % 10;
total = hangChuc + donVi;
console.log(total);
